import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maindashboard',
  templateUrl: './maindashboard.component.html',
  styles: []
})
export class MaindashboardComponent implements OnInit {

  
  constructor() {
    
   }

  ngOnInit() {

    this.loadInitScripts();
  }


  // loading scripts
  loadInitScripts() {
    
    this.loadScript('assets/js/plugins.js');
    this.loadScript('assets/js/custom-script.js');
    this.loadScript('assets/js/customizer.js');
  }

  loadScript(url: string) {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

}
