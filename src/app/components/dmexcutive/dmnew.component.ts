import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { DmExcutiveService } from 'src/app/shared/services/dm-excutive.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DmNewService } from 'src/app/shared/services/dm-new.service';
import { CoordinateNewService } from 'src/app/shared/serivces/coordinate-new.service';
import { Category } from 'src/app/shared/models/categories';
import { DMServices } from './DMServices';

@Component({
  selector: 'app-dmnew',
  templateUrl: './dmnew.component.html',
  styles: []
})
export class DMnewComponent implements OnInit {
  Category:Category[]= [];
  urls = [];
  services:DMServices[] = [];
  frmDmExcutive:FormGroup;
  _id:number;
  constructor(private _fb:FormBuilder,
    private _service:DmNewService,
    private _catservice:CoordinateNewService,
        private _router:Router,
        private _avrouter:ActivatedRoute) { }

  ngOnInit() {
    this.loadInitScripts();
    this.getCategories();

    this.frmDmExcutive = this._fb.group({
    eventStatusId: new FormControl(''),
    eventServiceIdsAndData: new FormControl(''),
    isPhotoUploaded: new FormControl(''),
    C_photoUploadedPath: new FormControl(''),
    isVideoUploaded: new FormControl(''),
    videoUploadedPath: new FormControl(''),
    C_eventName: new FormControl(''),
    C_eventCategoryId: new FormControl(''),
    C_startDateTime: new FormControl(''),
    C_endDateTime: new FormControl(''),
    C_venue: new FormControl(''),
    C_guests: new FormControl(''),
    C_speakers: new FormControl(''),
    C_participants:new FormControl(''),
    C_eventDescription: new FormControl(''),
    C_desciption : new FormControl(''),
    isDMCompleted: new FormControl(''),
    DMComments: new FormControl(''),
    isEventLockReleased: new FormControl(''),
    eventLockReleaseReason: new FormControl(''),
    eventDescription:new FormControl(''),
    multitext: this._fb.array([
      this.initmulti(),
    ]),
  //dm
    eventId: new FormControl(''),
    clientId: new FormControl(''),
    callingFrom:new FormControl(''),

    EventName: new FormControl(''),
    EventCategoryId: new FormControl(''),
    StartDateTime: new FormControl(''),
    EndDateTime: new FormControl(''),
    Venue: new FormControl(''),
    Guests: new FormControl(''),
    Speakers: new FormControl(''),
    Participants: new FormControl(''),
    })

    //get data by id
    this._avrouter.paramMap.subscribe(
      (param) =>{
        this._id = +param.get('id');
        if(this._id > 0){
          this._service.getEventById(this._id).subscribe(
            (data:any) =>{
              const res = data.Responce[0];
             // this.frmDmExcutive.controls['ClientId'].setValue(res.ClientId);
              this.frmDmExcutive.controls['C_eventName'].setValue(res.EventName);
              this.frmDmExcutive.controls['C_eventCategoryId'].setValue(res.EventCategoryId);
              this.frmDmExcutive.controls['C_startDateTime'].setValue(res.StartDateTime);
              this.frmDmExcutive.controls['C_endDateTime'].setValue(res.EndDateTime);
              this.frmDmExcutive.controls['C_venue'].setValue(res.Venue);
              this.frmDmExcutive.controls['C_guests'].setValue(res.Guests);
              this.frmDmExcutive.controls['C_speakers'].setValue(res.Speakers);
              this.frmDmExcutive.controls['C_participants'].setValue(res.Participants);
              this.frmDmExcutive.controls['C_desciption'].setValue(res.EventStatusDescription);
              this.frmDmExcutive.controls['C_photoUploadedPath'].setValue(res.PhotosPath);
              this.getServices();
            }
          )
        }
      }
    )

  }

  get DmFrm(){ return this.frmDmExcutive.controls};

   // Get Categories
   getCategories(){
    this._catservice.getCategory().subscribe(
      (data:any) =>{
        this.Category = data.Responce;
      },
      (error:any)=> {console.log(error)}
    )
  }

  //Images
  onSelectFile(event){
    if(event.target.files && event.target.files[0]){
      var fileslen  = event.target.files.length;
        for(var i=0; fileslen; i++){

          var reader = new FileReader();
          reader.onload = (event:any) =>{
            console.log(event.target.result);
            this.urls.push(event.target.result);
          }
          var name = event.target.files[i].name;
          var type = event.target.files[i].type;
          var size = event.target.files[i].size;

          console.log('Name :' + name + "\n" +
                      'Type :' + type + "\n" +
                      'Size :' + Math.round(size/1024) + "KB"
                      );

          reader.readAsDataURL(event.target.files[i]);
          console.log(event.target.files[i].name);
        }
    }
  }

  //Dynamic textbox
  initmulti(){
    return this._fb.group({
      Facebook: new FormControl(),
    })
  }

  addLanguages(){
    const ctrl = <FormArray>this.frmDmExcutive.controls['multitext'];
    ctrl.push(this.initmulti());
  }

  removeLonguages(i:number){
    const ctrl = <FormArray>this.frmDmExcutive.controls['multitext'];
    ctrl.removeAt(i);
  }

  //DM Services
  getServices(){
    var callingFrom=3;
      this._service.getDMServices(this._id,callingFrom).subscribe(
        (data:any) =>{

          this.services = data.Responce;
          console.log(this.services);
        }
      )
  }

  //DM Save
  onSave():void{

    this.frmDmExcutive.controls['eventStatusId'].setValue('20');
      this._service.saveDMExcutive(this.frmDmExcutive.value).subscribe(
          (data) =>{

            alert('Data Saved Successfully...');
            console.log(data);
          },
          (error)=>{ console.log(error)}
      )
  }

  onClose(){
      this._router.navigate(['/maindash', 'dmexcutivenet'])

  }
  // loading scripts
  loadInitScripts() {

    this.loadScript('assets/js/plugins.js');
    this.loadScript('assets/js/custom-script.js');
    this.loadScript('assets/js/customizer.js');
  }

  loadScript(url: string) {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }
}
