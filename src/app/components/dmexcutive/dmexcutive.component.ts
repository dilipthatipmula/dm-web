import { Component, OnInit } from '@angular/core';
import { ClientCoordianterService } from 'src/app/shared/services/client-coordianter.service';
import { EventStatus, coClients } from 'src/app/shared/models/EventStatus';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { userClients } from 'src/app/shared/models/userClients';

@Component({
  selector: 'app-dmexcutive',
  templateUrl: './dmexcutive.component.html',
  styles: []
})
export class DmexcutiveComponent implements OnInit {

  summmery:any;
  Status:EventStatus[]= [];
  clients:coClients[] = [];
  events:Event[] = [];
  eventgriddata:any[] = [];

  public placeholder: string = 'Enter the Event Name';
  public keyword = '';


  eventarray:any = [];

  public countriesReactive = this.eventarray;

  EventCoordinater:FormGroup;
  constructor(private _dashservice:ClientCoordianterService,
                       private _fb:FormBuilder) { }

  ngOnInit() {
    this.getSummeryDetails();
    this.getEventStatus();
    this.getClients();
   // this.getEvent();
    this.EventCoordinater = this._fb.group({
      eventName:new FormControl(''),
      startDate_From:new FormControl(''),
      eventStatusId:new FormControl(''),
      startDate_To:new FormControl(''),
      Venue:new FormControl(''),
      guests:new FormControl(''),
      eventId:new FormControl(''),
      clientId:new FormControl(''),
      orderByColumn:new FormControl(''),
      orderAscDesc:new FormControl(''),
      pageLength:new FormControl(''),
      pageIndex:new FormControl(''),
    })
  }

  get CoordinaterEv(){return this.EventCoordinater.controls}

  getSummeryDetails(){
    this._dashservice.getEventSummery().subscribe(
      (data:any) =>{
        this.summmery = data.Responce[0];
      },
      (error:any)=> {console.log(error)}
    )
  }

  getEventStatus(){
    this._dashservice.getEventStatus().subscribe(
      (data:any) =>{
        this.Status = data.Responce;
      },
      (error:any)=> {console.log(error)}
    )
  }

  getClients(){
    this._dashservice.getClients().subscribe(
      (data:any) =>{
        this.clients = data.Responce;
        console.log("clients for the user "+JSON.stringify(data));
      },
      (error:any)=> {console.log(error)}
    )
  }

  //autofill
  onChangeSearch(data:string, clientId:string){
    clientId="1";
    this._dashservice.EventAutofilter(data, clientId).subscribe(
      (res:any) =>{
        for(var i=0; i<res.Responce.length; i++)
        {
          this.eventarray.push(res.Responce[i].EventName);
        }

      }
    )
  }

  OngetEvents(){

    var callingFrom = "1";
    this._dashservice.getEventsGrid(callingFrom,this.EventCoordinater.value).subscribe(
      (data:any) =>{

        this.eventgriddata = data.Responce;
        console.log("Events for the grid "+JSON.stringify(data));
      },
      (error) =>{
        console.log(error);
      }
    )
  }

}
