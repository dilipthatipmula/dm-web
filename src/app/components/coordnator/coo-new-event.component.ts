import { Component, OnInit } from '@angular/core';
import { CoordinateNewService } from 'src/app/shared/serivces/coordinate-new.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Clientservice } from 'src/app/shared/models/EventStatus';
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/shared/models/categories';

@Component({
  selector: 'app-coo-new-event',
  templateUrl: './coo-new-event.component.html',
  styles: []
})
export class CooNewEventComponent implements OnInit {

  Category: Category[] = [];
  Services: Clientservice[] = [];
  frmManageEvent: FormGroup;
  _id: number;

  result: any = '';
  constructor(private _service: CoordinateNewService,
    private _fb: FormBuilder,
    private _router: Router,
    private _avrouter: ActivatedRoute) {
  }

  // init
  ngOnInit() {
    this.getCategories();
    this.getServices();

    this.frmManageEvent = this._fb.group({
      eventId: new FormControl(''),
      eventName: new FormControl(''),
      eventCategoryId: new FormControl(''),
      startDateTime: new FormControl(''),
      endDateTime: new FormControl(''),
      venue: new FormControl(''),
      guests: new FormControl(''),
      speakers: new FormControl(''),
      participants: new FormControl(''),
      eventDescription: new FormControl(''),
      eventStatusId: new FormControl(''),
      serviceIdsOpted: new FormControl(''),
      isSubmitedForDM: new FormControl(''),
      eventStatusDescription: new FormControl(''),
      newStartDateTime: new FormControl(''),
      newEndDateTime: new FormControl(''),
      isPhotoUploaded: new FormControl(''),
      photoUploadedPath: new FormControl(''),
    });




  }

  get frm() { return this.frmManageEvent.controls }
  get serviceIdsOpted() { return this.frmManageEvent.get('serviceIdsOpted') }
  get eventId() { return this.frmManageEvent.get('eventId') }
  // complete checkbox
  compChange(evt) {
    var target = evt.target;
    if (target.checked) {
      document.getElementById('Ns').style.display = 'none';
      document.getElementById('Ne').style.display = 'none';
    } else {
      document.getElementById('Ns').style.display = 'block';
      document.getElementById('Ne').style.display = 'block';
    }
  }

  //postpone checkbox
  postChange(evt) {
    var target = evt.target;
    if (target.checked) {
      document.getElementById('Ns').style.display = 'block';
      document.getElementById('Ne').style.display = 'block';
    }
  }

  //cancel checkbox
  cancelChange(evt) {
    var target = evt.target;
    if (target.checked) {
      document.getElementById('Ns').style.display = 'none';
      document.getElementById('Ne').style.display = 'none';
    }
  }

  // Get Categories
  getCategories() {
    console.log("Get categories ");
    this._service.getCategory().subscribe(
      (data: any) => {
        this.Category = data.Responce;
      },
      (error: any) => { console.log(error) }
    )
  }

  // Get Service
  getServices() {
    this._service.getServices().subscribe(
      (data: any) => {
        this.Services = data.Responce;
      },
      (error: any) => { console.log(error) }
    )
  }

  //Submit Form
  onSubmit(): void {
    this.frmManageEvent.controls['serviceIdsOpted'].setValue(this.result);

    this._service.saveEvent(this.frmManageEvent.value).subscribe(
      (data) => {
        alert("Data Saved Succesfully");
        //console.log(data);
        this.frmManageEvent.reset();
      })
  }

  onDmSubmit() {
    this.frmManageEvent.controls['isSubmitedForDM'].setValue('1');
    this.frmManageEvent.controls['serviceIdsOpted'].setValue(this.result);
    this._service.saveEvent(this.frmManageEvent.value).subscribe(
      (data) => {

        alert("Data Saved Succesfully");
        console.log(data);
        this.frmManageEvent.reset();
      })
  }


  //checkboxes checked

  public onChange(option, event) {
    if (event.target.checked == true) {
      this.result += event.target.value + ",";
    }
    else {
      this.result = this.result.replace(event.target.value + ',', '');
    }
  }

  onClose() {
    this._router.navigate(['/maindash', 'coordinaterevent'])
  }

}
