import { Component, OnInit } from '@angular/core';
import { CoordinateNewService } from 'src/app/shared/serivces/coordinate-new.service';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { CManageEvent } from 'src/app/shared/models/CManageEvent';
import { Clientservice } from 'src/app/shared/models/EventStatus';
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/shared/models/categories';

@Component({
  selector: 'app-coo-edit-event',
  templateUrl: './coo-edit-event.component.html',
  styles: []
})
export class CooEditEventComponent implements OnInit {

  Category: Category[] = [];
  Services: Clientservice[] = [];
  frmManageEvent: FormGroup;
  _id: number;
  eventServices: Clientservice[] = [];

  result: any = '';
  constructor(private _service: CoordinateNewService,
    private _fb: FormBuilder,
    private _router: Router,
    private _avrouter: ActivatedRoute) {
  }

  // init
  ngOnInit() {
    this.getCategories();

    this.loadInitScripts();
    this.frmManageEvent = this._fb.group({
      eventId: new FormControl(''),
      eventName: new FormControl(''),
      eventCategoryId: new FormControl(''),
      startDateTime: new FormControl(''),
      endDateTime: new FormControl(''),
      venue: new FormControl(''),
      guests: new FormControl(''),
      speakers: new FormControl(''),
      participants: new FormControl(''),
      eventDescription: new FormControl(''),
      eventStatusId: new FormControl(''),
      serviceIdsOpted: new FormControl(''),
      isSubmitedForDM: new FormControl(''),
      eventStatusDescription: new FormControl(''),
      newStartDateTime: new FormControl(''),
      newEndDateTime: new FormControl(''),
      isPhotoUploaded: new FormControl(''),
      photoUploadedPath: new FormControl(''),
    });


    this._avrouter.paramMap.subscribe(
      (param) => {

        this._id = +param.get('id');
        if (this._id > 0) {
          this._service.getEventById(this._id).subscribe(
            (data: any) => {

              console.log('Data for the ID selected ', JSON.stringify(data));
              const res = data.Responce[0];
              this.frmManageEvent.controls['eventId'].setValue(res.EventId);
              this.frmManageEvent.controls['eventName'].setValue(res.EventName);
              this.frmManageEvent.controls['eventCategoryId'].setValue(res.EventCategoryId);
              this.frmManageEvent.controls['startDateTime'].setValue(res.StartDateTime);
              this.frmManageEvent.controls['endDateTime'].setValue(res.EndDateTime);
              this.frmManageEvent.controls['venue'].setValue(res.Venue);
              this.frmManageEvent.controls['guests'].setValue(res.Guests);
              this.frmManageEvent.controls['speakers'].setValue(res.Speakers);
              this.frmManageEvent.controls['participants'].setValue(res.Participants);
              this.frmManageEvent.controls['eventDescription'].setValue(res.EventDescription);
              this.frmManageEvent.controls['eventStatusId'].setValue(res.EventStatusId);
              this.frmManageEvent.controls['serviceIdsOpted'].setValue(res.serviceIdsOpted);
              this.frmManageEvent.controls['isSubmitedForDM'].setValue(res.IsSubmitedForDM);
              this.frmManageEvent.controls['eventStatusDescription'].setValue(res.EventStatusDescription);
              this.frmManageEvent.controls['newStartDateTime'].setValue(res.PostponedStartDateTime);
              this.frmManageEvent.controls['newEndDateTime'].setValue(res.PostponedEndDateTime);
              this.frmManageEvent.controls['isPhotoUploaded'].setValue(res.IsPhotoUploaded);
              this.frmManageEvent.controls['photoUploadedPath'].setValue(res.PhotosPath);

              this.getEventServices();
              this.getServices();
              if (res.EventStatusId !== '10') {
                document.getElementById('submitDMbtn').style.display = 'none';
              }
              if (res.EventStatusId >= '15') {
                document.getElementById('savebtn').style.display = 'none';
              }
            }
          );
        }

      }
    );

  }

  get frm() { return this.frmManageEvent.controls; }
  get serviceIdsOpted() { return this.frmManageEvent.get('serviceIdsOpted'); }
  get eventId() { return this.frmManageEvent.get('eventId'); }
  // complete checkbox
  compChange(evt) {
    const target = evt.target;
    if (target.checked) {
      document.getElementById('Ns').style.display = 'none';
      document.getElementById('Ne').style.display = 'none';
      document.getElementById('submitDMbtn').style.display = 'block';
    } else {
      document.getElementById('Ns').style.display = 'block';
      document.getElementById('Ne').style.display = 'block';
      document.getElementById('submitDMbtn').style.display = 'none';
    }
  }

  // postpone checkbox
  postChange(evt) {
    const target = evt.target;
    if (target.checked) {
      document.getElementById('Ns').style.display = 'block';
      document.getElementById('Ne').style.display = 'block';
      document.getElementById('submitDMbtn').style.display = 'none';
    }
  }

  // cancel checkbox
  cancelChange(evt) {
    const target = evt.target;
    if (target.checked) {
      document.getElementById('Ns').style.display = 'none';
      document.getElementById('Ne').style.display = 'none';
      document.getElementById('submitDMbtn').style.display = 'none';
    }
  }

  // Get Categories
  getCategories() {
    console.log('Get categories ');
    this._service.getCategory().subscribe(
      (data: any) => {
        this.Category = data.Responce;
      },
      (error: any) => { console.log(error); }
    );
  }

  // Get Service
  getServices() {
    this._service.getServices().subscribe(
      (data: any) => {
        this.Services = data.Responce;
        const serlen = this.Services.length;
        const len = this.eventServices.length;
        let i = 0, j = 0;
        for (i = 0; i <= serlen; i++) {
          this.Services[i].checked = false;
          for (j = 0; j <= len; j++) {
            if (this.Services[i].ServiceId === this.eventServices[j].ServiceId) {
              this.Services[i].checked = true;

            }
            break;
          }

        }
        console.log('service ' + JSON.stringify(this.Services));
      },
      (error: any) => { console.log(error); }
    );
  }

  // getEventServices
  getEventServices() {
    const callingFrom = '2';

    this._service.getEventServices(this._id, callingFrom).subscribe(
      (data: any) => {
        console.log('Event services ' + JSON.stringify(data));
        this.eventServices = data.Responce;
      }
    );
  }
  // Submit Form
  onSubmit(): void {
    this.frmManageEvent.controls['serviceIdsOpted'].setValue(this.result);
    this._service.saveEvent(this.frmManageEvent.value).subscribe(
      (data) => {

        alert('Data Saved Succesfully');
        console.log(data);
        this._router.navigate(['/maindash', 'coordinaterevent']);
        // this.frmManageEvent.reset();
      });
  }

  onDmSubmit() {
    this.frmManageEvent.controls['isSubmitedForDM'].setValue('1');
    this.frmManageEvent.controls['serviceIdsOpted'].setValue(this.result);
    this.frmManageEvent.controls['eventStatusId'].setValue('15');
    this._service.saveEvent(this.frmManageEvent.value).subscribe(
      (data) => {

        alert('Data Saved Succesfully');
        console.log(data);
        this._router.navigate(['/maindash', 'coordinaterevent']);
        // this.frmManageEvent.reset();
      });
  }


  // checkboxes checked

  public onChange(option, event) {
    if (event.target.checked === true) {
      this.result += event.target.value + ',';
    } else {
      this.result = this.result.replace(event.target.value + ',', '');
    }
  }

  onClose() {
    this._router.navigate(['/maindash', 'coordinaterevent']);
  }

  loadInitScripts() {

    this.loadScript('assets/js/plugins.js');
    this.loadScript('assets/js/custom-script.js');
    this.loadScript('assets/js/customizer.js');
  }

  loadScript(url: string) {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

}
