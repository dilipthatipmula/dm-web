import { Component, OnInit } from '@angular/core';
import { EventStatus, coClients, Event } from 'src/app/shared/models/EventStatus';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CooDashboardEvent } from 'src/app/shared/models/cooDashEvent';
import { ClientCoordianterService } from 'src/app/shared/services/client-coordianter.service';

@Component({
  selector: 'app-coordinater-event',
  templateUrl: './coordinater-event.component.html',
  styles: [`
  .autocomplete-container {
    height: 59px !important;
}`
  ]
})
export class CoordinaterEventComponent implements OnInit {

  summmery: any;
  Status: EventStatus[] = [];
  gender: string[];
  clients: coClients[] = [];
  events: Event[] = [];
  eventGrid: CooDashboardEvent[] = [];
  eventgriddata: any[] = [];
  client_id: string;


  public placeholder = 'Enter the Event Title';
  public keyword = '';


  eventarray: any = [];

  public countriesReactive = this.eventarray;

  EventCoordinater: FormGroup;
  constructor(private _service: ClientCoordianterService,
    private _fb: FormBuilder) {

  }

  ngOnInit() {
    this.getSummeryDetails();
    this.getEventStatus();
    this.getClients();

    // this.getEvent();
    this.EventCoordinater = this._fb.group({
      eventName: new FormControl(''),
      startDate_From: new FormControl(''),
      eventStatusId: new FormControl(''),
      startDate_To: new FormControl(''),
      Venue: new FormControl(''),
      guests: new FormControl(''),
      eventId: new FormControl(''),
      clientId: new FormControl(''),
      orderByColumn: new FormControl(''),
      orderAscDesc: new FormControl(''),
      pageLength: new FormControl(''),
      pageIndex: new FormControl(''),
      callingFrom: new FormControl(''),
      // name: ['', Validators.required]

    });
    this.OngetEvents();
  }

  get eventName() { return this.EventCoordinater.get('eventName'); }
  get clientId() { return this.EventCoordinater.get('clientId'); }

  getSummeryDetails() {
    this._service.getEventSummery().subscribe(
      (data: any) => {
        this.summmery = data.Responce[0];
      },
      (error: any) => { console.log(error); }
    );
  }

  get CoordinaterEv() { return this.EventCoordinater.controls; }

  getEventStatus() {
    this._service.getEventStatus().subscribe(
      (data: any) => {
        this.Status = data.Responce;
      },
      (error: any) => { console.log(error); }
    );
  }

  getClients() {
    this._service.getClients().subscribe(
      (data: any) => {
        this.clients = data.Responce;
      },
      (error: any) => { console.log(error); }
    );
  }

  // autofill
  onChangeSearch(data: string, clientId: string) {
    clientId = '1';
    this._service.EventAutofilter(data, clientId).subscribe(
      (res: any) => {
        for (let i = 0; i < res.Responce.length; i++) {
          this.eventarray.push(res.Responce[i].EventName);
        }
      }
    );
  }

  OngetEvents() {

    const callingFrom = '2';
    // this.EventCoordinater.controls[''].setValue('0');
    this._service.getEventsGrid(callingFrom, this.EventCoordinater.value).subscribe(
      (data: any) => {

        this.eventgriddata = data.Responce;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
