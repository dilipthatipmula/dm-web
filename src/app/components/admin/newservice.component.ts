import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ClientCoordianterService } from 'src/app/shared/services/client-coordianter.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newservice',
  templateUrl: './newservice.component.html',
  styles: []
})
export class NewserviceComponent implements OnInit {
  serviceTypes: any;
  serviceForm: FormGroup;
  constructor(private _service: ClientCoordianterService,
    private fb: FormBuilder,
    private _router: Router) { }

  ngOnInit() {
    this.getServiceTypes();
    this.serviceForm = this.fb.group({
      serviceName: new FormControl('', Validators.required),
      serviceType: new FormControl('', Validators.required),
      active: new FormControl(true, Validators.required)
    });
  }
  getServiceTypes() {
    this.serviceTypes = [
      {
        'ServiceTypeId': '1',
        'ServiceTypeCode': 'URL',
        'ServiceTypeName': 'URL'
      },
      {
        'ServiceTypeId': '2',
        'ServiceTypeCode': 'Path',
        'ServiceTypeName': 'File Path'
      },
      {
        'ServiceTypeId': '3',
        'ServiceTypeCode': 'Text',
        'ServiceTypeName': 'Text'
      },
      {
        'ServiceTypeId': '4',
        'ServiceTypeCode': 'Gift',
        'ServiceTypeName': 'File Path'
      }
    ];
    this._service.getMeTheServiceTypes().subscribe(
      (data: any) => {
        // this.serviceTypes = data.Responce;

      },
      (error: any) => { console.log(error); }
    );
  }
  saveService() {
    console.log('...', this.serviceForm);
  }

  onClickClose() {
    this._router.navigate(['/maindash', 'services']);
  }

}
