export interface User {
    UserId: any;
    RoleId: any;
    Role: any;
    ClientId: any;
    ClientName: any;
    Name: any;
    ContactNo: any;
    eMail: any;
    Designation: any;
    Active: any;
    Status: any;
    LastModifiedOn: any;
    LastModifiedBy: any;
}