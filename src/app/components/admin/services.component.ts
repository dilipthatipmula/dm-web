import { Component, OnInit } from '@angular/core';
import { ClientCoordianterService } from 'src/app/shared/services/client-coordianter.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styles: []
})
export class ServicesComponent implements OnInit {
  serviceList: any;
  serviceTypes = [
    {
      'ServiceTypeId': '1',
      'ServiceTypeCode': 'URL',
      'ServiceTypeName': 'URL'
    },
    {
      'ServiceTypeId': '2',
      'ServiceTypeCode': 'Path',
      'ServiceTypeName': 'File Path'
    },
    {
      'ServiceTypeId': '3',
      'ServiceTypeCode': 'Text',
      'ServiceTypeName': 'Text'
    },
    {
      'ServiceTypeId': '4',
      'ServiceTypeCode': 'Gift',
      'ServiceTypeName': 'File Path'
    }
  ];
  constructor(private _service: ClientCoordianterService) { }

  ngOnInit() {

    this.getServiceList();
  }

  getServiceList() {
    this._service.getServices().subscribe(
      (data: any) => {
        this.serviceList = data.Responce;
      },
      (error: any) => { console.log(error); }
    );
  }

  onClickSearch() {

  }
}
