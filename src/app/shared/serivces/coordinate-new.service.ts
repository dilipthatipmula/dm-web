import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { Category } from '../models/categories';
import { CManageEvent } from '../models/CManageEvent';
import { Clientservice } from '../models/EventStatus';
import { getEvents } from '../models/getEvent';

@Injectable({
  providedIn: 'root'
})
export class CoordinateNewService {

  api: string;
  apipost: string;
  apiget: string;
  bearerauthtoken: string;
  constructor(private _http: HttpClient,
    private _serive: AuthService) {
    this.api = this._serive.apiUlr;
    this.apipost = this._serive.apiUlr + 'Events/manageEvent';
    this.apiget = this._serive.apiUlr + 'Events/getEvent';
    this.bearerauthtoken = 'Bearer ' + sessionStorage.getItem('secureToken');
  }

  getCategory(): Observable<Category[]> {
    console.log('Inside getCategory method ');
    return this._http.get<Category[]>(`${this.api}Events/eventCategories`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.bearerauthtoken
      })
    })
      .pipe(catchError(this.handleError));
  }

 /* getServices(): Observable<Clientservice[]> {
    return this._http.get<Clientservice[]>(`${this.api}Services/clientServices`)
      .pipe(catchError(this.handleError))
  } */


  getServices(): Observable<Event> {
    const obj = {
      clientId: '1',
      callingFrom: '2'
    };
    return this._http.post<Event>(`${this.api}Services/clientServices`, JSON.stringify(obj), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization' : this.bearerauthtoken
        })
    })
      .pipe(catchError(this.handleError));

  }


  getEventServices(eventId: any, callingFrom: any): Observable<any> {
    const obj = {
     eventId: eventId,
      callingFrom: callingFrom
    };
    return this._http.post<any>(`${this.api}Events/getEventServices`, obj , {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization' : this.bearerauthtoken
        })
    })
      .pipe(catchError(this.handleError));

  }



  saveEvent(data: CManageEvent): Observable<CManageEvent> {
    if (data.eventStatusId === '') {
        data.eventStatusId = 1;
    }
    console.log('save event ' + JSON.stringify(data));
    return this._http.post<CManageEvent>(`${this.apipost}`, JSON.stringify(data), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization' : this.bearerauthtoken
        })
    })
      .pipe(catchError(this.handleError));

  }

  getEventById(id: number): Observable<CManageEvent> {
    return this._http.post<CManageEvent>(`${this.apiget}/${id}`, {callingFrom: 2, eventId: id}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization' : this.bearerauthtoken
      })
    })
      .pipe(catchError(this.handleError));
  }

  handleError(error: any) {
    console.log(error);
    return Observable.throw(error);
  }

}
