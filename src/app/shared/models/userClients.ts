export interface userClients{
    ClientId: number;
    ClientName: string;
    IsClientAssigned: number;
}