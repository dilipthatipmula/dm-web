export class CManageEvent{
    eventName:any;
    eventCategoryId:any;
    startDateTime:any;
    endDateTime:any;
    venue:any;
    guests:any;
    speakers:any;
    participants:any;
    eventDescription:any;
    eventStatusId:any;
    serviceIdsOpted:any;
    isSubmitedForDM:any;
    eventStatusDescription:any;
    newStartDateTime:any;
    newEndDateTime:any;
    isPhotoUploaded:any;
    photoUploadedPath:any;
    eventId:any;
}