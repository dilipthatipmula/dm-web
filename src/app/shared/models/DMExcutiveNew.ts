export interface DMExcutiveNew{
eventId: any;
clientId: any;
eventStatusId: any;
eventServiceIdsAndData: any;
isPhotoUploaded: any;
photoUploadedPath: any;
isVideoUploaded: any;
videoUploadedPath: any;
eventName: any;
eventCategoryId: any;
startDateTime: any;
endDateTime: any;
venue: any;
guests: any;
speakers: any;
participants:any;
eventDescription: any;
isDMCompleted: any;
DMComments: any;
isEventLockReleased:any;
eventLockReleaseReason: any;
images_7:any[];
images_8:any[];
}