export interface getEvents{
    eventId:any;
    eventName:any;
    eventStatusId:any;
    Venue:any;
    guests:any;
    startDate_From:any;
    startDate_To:any;
    clientId:any;
    orderByColumn:any;
    orderAscDesc:any;
    pageLength:any;
    pageIndex:any;
    callingFrom:any;
}