export interface Category{
    EventCategoryId:number;
    CategoryName:string;
     Active:number;
     LastModifiedOn:string
      LastModifiedBy:string;
}