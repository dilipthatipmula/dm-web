export interface EventStatus{
    EventStatusId:number;
    EventStatus:string;
}

export interface coClients{
    ClientId:number;
    ClientName:string;
    IsClientAssigned:number;
}

export interface Event{
    EventId:string;
    EventName:string;
    ClientId: string;
}

export interface Clientservice{
    ServiceId:number;
    ServiceName:string;
    checked: boolean;
}