import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { HttpHeaders } from '@angular/common/http';
import { DMExcutiveNew } from '../models/DMExcutiveNew';
import { Category } from '../models/categories';
import { DMServices } from 'src/app/components/dmexcutive/DMServices';

@Injectable({
  providedIn: 'root'
})
export class DmNewService {

  url:string;
  apiget:string;
  bearerauthtoken : string;
  constructor(private _http:HttpClient,
                private _service:AuthService) {
                  this.url = this._service.apiUlr;
                  this.apiget = this._service.apiUlr + 'Events/getEvent'
                  this.bearerauthtoken = 'Bearer '+ sessionStorage.getItem('secureToken');
                 }

  saveDMExcutive(data:DMExcutiveNew): Observable<DMExcutiveNew> {

    console.log("Data for save dm executive "+JSON.stringify(data));
    return this._http.post<DMExcutiveNew>(`${this.url}events/dmmanageEvent`, data , {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': this.bearerauthtoken

        })
    })
      .pipe(catchError(this.handleError))

  }
  //services
  getDMServices(eventId,callingFrom): Observable<any> {

    var obj = {
     eventId:eventId,
     clientId:"1",
      callingFrom:callingFrom
    }
    return this._http.post<any>(`${this.url}Events/getEventServices`, obj , {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization':this.bearerauthtoken
        })
    })
      .pipe(catchError(this.handleError))

  }

  getEventById(id: number): Observable<DMExcutiveNew> {

    var obj = {
      eventId: id,
      eventName: "",
      eventStatusId: "",
      venue: "",
      guests: "",
      startDate_From: "",
      startDate_To: "",
      clientId: "",
      orderByColumn: "",
      orderAscDesc: "",
      pageLength: "",
      pageIndex: "",
      callingFrom: ""
    }
    return this._http.post<DMExcutiveNew>(`${this.apiget}/${id}`, obj,{
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .pipe(catchError(this.handleError))
  }

  getCategory(): Observable<Category[]> {
    return this._http.get<Category[]>(`${this.url}Events/eventCategories`)
      .pipe(catchError(this.handleError))
  }

  handleError(error: any) {

    console.log(error);
    return Observable.throw(error);
  }
}
