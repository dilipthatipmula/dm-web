import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { Services } from '../models/Services';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  url:string;
  constructor(private _http:HttpClient,
    private _service:AuthService) {
      this.url = this._service.apiUlr;
     }

     getServices(data:Services):Observable<Services>{

        return this._http.post<Services>(`${this.url}Services/getServices`, data,{
            headers:new HttpHeaders({
              'Content-Type':'applicatin/json'
            })
        })
        .pipe(catchError(this.handleError))
    }

    getServicesbyId(id:number):Observable<Services>{
      return this._http.get<Services>(`${this.url}/${id}`)
                .pipe(catchError(this.handleError))
    }

    handleError(error:any){
      console.log(error);
      return Observable.throw(error);

    }
}
