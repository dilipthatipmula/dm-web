import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { CooDashboardEvent } from '../models/cooDashEvent';
import { getEvents } from '../models/getEvent';
import { Event } from '../models/EventStatus';

@Injectable({
  providedIn: 'root'
})
export class ClientCoordianterService {


  users: string;
  events: string;
  cooGrid: string;
  api: string;
  bearerauthtoken: string;
  constructor(private _http: HttpClient,
    private _service: AuthService) {
    this.events = this._service.apiUlr + 'Events/eventNamesAjax';
    // this.cooGrid = this._service.apiUlr + 'Events/getEvent';
    this.api = this._service.apiUlr;
    this.bearerauthtoken = 'Bearer ' + sessionStorage.getItem('secureToken');
  }

  getEventSummery(): Observable<any[]> {
    var data = {
      clientId: "1"
    }
    return this._http.post<any[]>(`${this.api}Events/eventSummary`, data, {
      headers: new HttpHeaders({
        'content-type': 'application/json'
      })
    })
      .pipe(catchError(this.handleError))
  }

  getServices(): Observable<any[]> {
    return this._http.post<any[]>(`${this.api}Services/getServices`, { serviceId: 0 }, {
      headers: new HttpHeaders({
        'content-type': 'application/json'
      })
    })
      .pipe(catchError(this.handleError));
  }
  getMeTheServiceTypes(): Observable<any[]> {
    return this._http.get<any[]>(`${this.api}Services/getServiceType`)
      .pipe(catchError(this.handleError));
  }


  getClients(): Observable<any[]> {
    return this._http.get<any[]>(`${this.api}User/userClients`)
      .pipe(catchError(this.handleError))
  }

  getEventStatus(): Observable<any[]> {
    return this._http.get<any[]>(`${this.api}Events/eventStatus`)
      .pipe(catchError(this.handleError))
  }

  getEvents(event: Event): Observable<any[]> {
    return this._http.post<any[]>(`${this.events}`, event, {
      headers: new HttpHeaders({
        'content-type': 'application/json',
        'Authorization': this.bearerauthtoken
      })
    })
      .pipe(catchError(this.handleError))
  }


  // Event Autofilter
  EventAutofilter(data: string, clientId: string): Observable<Event> {

    clientId = '1'
    return this._http.post<Event>(`${this.api}Events/eventNamesAjax`, { data, clientId }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
      .pipe(catchError(this.handleError))
  }

  // Get Events Dashboard
  getEventsGrid(callingFrom, data: getEvents): Observable<getEvents> {
    data.callingFrom = callingFrom;
    console.log('Get events grid service' + JSON.stringify(data));
    return this._http.post<getEvents>(`${this.api}Events/getEvent`, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.bearerauthtoken
      })
    })
      .pipe(catchError(this.handleError))
  }

  handleError(error: any) {

    console.log(error);
    return Observable.throw(error);

  }
}
