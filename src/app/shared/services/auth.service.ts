import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginUser } from '../models/login-user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  redirectUrl: string;
  loginStatus: EventEmitter<boolean> = new EventEmitter<boolean>();
  public apiUlr = 'http://dm.khyathiupvc.com/api/';

  constructor(private _http: HttpClient,
    private _router: Router) { }

  public Login(params): Observable<LoginUser> {
    //
    const data = { username: params.userName, password: params.password };
    return this._http.post<LoginUser>(`${this.apiUlr}auth/login`, data);
  }



  public getUserDetails() {
    if (sessionStorage.getItem('user')) {
      return JSON.parse(sessionStorage.getItem('user'))
    } else {
      return null;
    }
  }

  get isLoggedin() { return !!sessionStorage.getItem('secureToken') }
  get getToken() { return sessionStorage.getItem('secureToken') }
  get refreshToken() { return sessionStorage.getItem('refreshToken') }

  public ManageSession(data: LoginUser) {
    sessionStorage.setItem('secureToken', data.token);
    sessionStorage.setItem('refreshToken', data.refresh_token);
    sessionStorage.setItem('user', JSON.stringify(data));
  }

  public LogOut() {
    this.redirectUrl = window.location.pathname;
    sessionStorage.removeItem('secureToken');
    sessionStorage.removeItem('refreshToken');
    sessionStorage.removeItem('user');
    this.loginStatus.emit(false);
  }

}
