import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CoordinateNewService } from 'src/app/shared/serivces/coordinate-new.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Clientservice } from 'src/app/shared/models/EventStatus';
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/shared/models/categories';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {

  Category: Category[] = [];
  eventObj: any = {};
  Services: Clientservice[] = [];
  eventStatusList: any;
  eventForm: FormGroup;
  @Output() getEventsList = new EventEmitter();
  actionLable: any;
  _id: number;
  @Input() showEventForm: any;
  @Input() selectedEvent: any;
  @Input() isDisplay = false;
  @Output() isDisplayChange = new EventEmitter<boolean>();
  selectedServices: any = '';
  constructor(private _service: CoordinateNewService,
    private _fb: FormBuilder,
    private _router: Router,
    private _avrouter: ActivatedRoute) {
  }

  // init
  ngOnInit() {
    this.getCategories();
    this.getServices();
    this.checkActionMode();
  }

  prepareNewForm() {
    console.log('for new');

    this.eventForm = this._fb.group({
      eventName: new FormControl('', Validators.required),
      eventCategoryId: new FormControl('', Validators.required),
      startDateTime: new FormControl('', Validators.required),
      endDateTime: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      guests: new FormControl('', Validators.required),
      speakers: new FormControl('', Validators.required),
      participants: new FormControl('', Validators.required),
      eventDescription: new FormControl('', Validators.required),
      serviceIdsOpted: new FormControl(''),
    });

  }
  prepareFormForUpdate() {
    console.log('for update');

    this.eventForm = this._fb.group({
      eventName: new FormControl('', Validators.required),
      eventCategoryId: new FormControl('', Validators.required),
      startDateTime: new FormControl('', Validators.required),
      endDateTime: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      guests: new FormControl('', Validators.required),
      speakers: new FormControl('', Validators.required),
      participants: new FormControl('', Validators.required),
      eventDescription: new FormControl('', Validators.required),
      serviceIdsOpted: new FormControl(''),
      eventStatus: new FormControl(''),
      photoUploadedPath: new FormControl(''),
      videoUploadedPath: new FormControl(''),
      newStartDateTime: new FormControl(''),
      newEndDateTime: new FormControl(''),
      eventStatusDescription: new FormControl('')
    });
    this.getEventData();
  }

  get diagnostic() { return JSON.stringify(this.eventForm.value); }
  checkActionMode() {
    console.log('checkaction', this.selectedEvent);
    if (this.selectedEvent) {
      this.getEventServices();
      this.actionLable = 'UPDATE';
      this.prepareFormForUpdate();
    } else {
      this.actionLable = 'SAVE';
      this.prepareNewForm();
    }
  }

  onChangeOptions(status) {
    console.log('status>>', status);

    this.eventStatusList.forEach(element => {
      if (element.value === status.value) {
        element.selected = true;
      }
    });
  }

  getEventData() {
    this._service.getEventById(this.selectedEvent['EventId']).subscribe(
      (data: any) => {
        this.eventObj = data.Responce[0];
        this.selectedEvent = data.Responce[0];
        this.eventObj.eventName = this.selectedEvent.EventName;
        this.eventObj.startDateTime = new Date(this.selectedEvent.StartDateTime);
        this.eventObj.eventCategoryId = this.selectedEvent.EventCategoryId;
        this.eventObj.endDateTime = this.selectedEvent.EndDateTime;
        this.eventObj.venue = this.selectedEvent.Venue;
        this.eventObj.guests = this.selectedEvent.Guests;
        this.eventObj.speakers = this.selectedEvent.Speakers;
        this.eventObj.participants = this.selectedEvent.Participants;
        this.eventObj.eventStatus = this.selectedEvent.EventStatusId;
        this.eventObj.serviceIdsOpted = this.selectedEvent.ServiceIdsOpted;
        this.eventObj.eventDescription = this.selectedEvent.EventDescription;
        this.eventObj.photoUploadedPath = this.selectedEvent.PhotosPath;
        this.eventObj.videoUploadedPath = this.selectedEvent.VideosPath;
        this.eventObj.eventStatusDescription = this.selectedEvent.EventStatusDescription;
        console.log('this.eventForm', this.eventForm);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getEventServices() {
    this._service.getEventServices(this.selectedEvent['EventId'], 2).subscribe(
      (data: any) => {
        data.Responce.forEach(element1 => {
          this.Services.forEach(element2 => {
            if (element1['ServiceId'] === element2['ServiceId']) {
              this.selectedServices += element1.ServiceId + ',';
              element2['checked'] = true;
            }
          });
        });

      },
      (error) => {
        console.log(error);
      }
    );
  }

  // Get Categories
  getCategories() {
    this._service.getCategory().subscribe(
      (data: any) => {
        this.Category = data.Responce;
      },
      (error: any) => { console.log(error); }
    );
  }

  // Get Service
  getServices() {
    this._service.getServices().subscribe(
      (data: any) => {
        this.Services = data.Responce;
        this.Services.forEach(element => {
          element['checked'] = false;
        });
      },
      (error: any) => { console.log(error); }
    );
  }

  // Submit Form
  onSubmit(): void {
    const form = this.eventForm.value;
    form['serviceIdsOpted'] = this.selectedServices;
    if (this.actionLable === 'UPDATE') {
      form['eventId'] = this.selectedEvent.EventId;
      form['isSubmitedForDM'] = 0;
      form['eventStatusId'] = this.eventObj.eventStatus;
    } else {
      form['eventId'] = 0;
      form['isSubmitedForDM'] = 0;
      form['eventStatusId'] = 1;
    }

    this._service.saveEvent(form).subscribe(
      (data) => {
        alert('Data Saved Succesfully');
        this.eventForm.reset();
        this.isDisplay = false;
        this.isDisplayChange.emit(this.isDisplay);
        this.getEventsList.emit();
      });
  }

  onDmSubmit() {
    this.eventForm.controls['isSubmitedForDM'].setValue('1');
    this.eventForm.controls['serviceIdsOpted'].setValue(this.selectedServices);
    this._service.saveEvent(this.eventForm.value).subscribe(
      (data) => {
        alert('Data Saved Succesfully');
        console.log(data);
        this.eventForm.reset();
      });
  }


  // checkboxes checked

  public onCheckServic(service, event) {
    console.log('this.selectedServices', this.selectedServices);
    if (event.target.checked === true) {
      this.selectedServices += event.target.value + ',';
    } else {
      this.selectedServices = this.selectedServices.replace(event.target.value + ',', '');
    }


  }

  onClose() {
    this.isDisplay = false;
    this.isDisplayChange.emit(this.isDisplay);
    this.selectedEvent = undefined;
  }

}
