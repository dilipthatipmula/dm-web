import { Component, OnInit } from '@angular/core';
import { EventStatus, coClients, Event } from 'src/app/shared/models/EventStatus';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CooDashboardEvent } from 'src/app/shared/models/cooDashEvent';
import { ClientCoordianterService } from 'src/app/shared/services/client-coordianter.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  summmery: any;
  Status: EventStatus[] = [];
  sortOrder = 0;
  gender: string[];
  clients: coClients[] = [];
  events: Event[] = [];
  eventGrid: CooDashboardEvent[] = [];
  eventgriddata: any[] = [];
  client_id: string;
  showEventForm = false;
  selectedEvent: any;


  public placeholder = 'Enter the Event Title';
  public keyword = '';


  eventarray: any = [];

  public countriesReactive = this.eventarray;

  EventCoordinater: FormGroup;
  constructor(private _service: ClientCoordianterService,
    private _fb: FormBuilder) {

  }

  ngOnInit() {
    this.getSummeryDetails();
    this.getEventStatus();
    this.getClients();

    // this.getEvent();
    this.EventCoordinater = this._fb.group({
      eventName: new FormControl(''),
      startDate_From: new FormControl(''),
      eventStatusId: new FormControl(''),
      startDate_To: new FormControl(''),
      Venue: new FormControl(''),
      guests: new FormControl(''),
      eventId: new FormControl(''),
      clientId: new FormControl(''),
      orderByColumn: new FormControl(''),
      orderAscDesc: new FormControl(''),
      pageLength: new FormControl(''),
      pageIndex: new FormControl(''),
      callingFrom: new FormControl('')
    });
    this.getEventsList();
  }

  get eventName() { return this.EventCoordinater.get('eventName'); }
  get clientId() { return this.EventCoordinater.get('clientId'); }

  getSummeryDetails() {
    this._service.getEventSummery().subscribe(
      (data: any) => {
        this.summmery = data.Responce[0];
      },
      (error: any) => { console.log(error); }
    );
  }

  get CoordinaterEv() { return this.EventCoordinater.controls; }

  getEventStatus() {
    this._service.getEventStatus().subscribe(
      (data: any) => {
        this.Status = data.Responce;
      },
      (error: any) => { console.log(error); }
    );
  }

  getClients() {
    this._service.getClients().subscribe(
      (data: any) => {
        this.clients = data.Responce;
      },
      (error: any) => { console.log(error); }
    );
  }

  // autofill
  onChangeSearch(data: string, clientId: string) {
    clientId = '1';
    this._service.EventAutofilter(data, clientId).subscribe(
      (res: any) => {
        for (let i = 0; i < res.Responce.length; i++) {
          this.eventarray.push(res.Responce[i].EventName);
        }
      }
    );
  }

  getEventsList() {
    const callingFrom = '2';
    this._service.getEventsGrid(callingFrom, this.EventCoordinater.value).subscribe(
      (data: any) => {
        this.eventgriddata = data.Responce;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  addEvent() {
    this.showEventForm = true;
    this.selectedEvent = undefined;
  }

  onClickClose() {
    this.showEventForm = false;
  }

  onClickEdit(event: any) {
    this.showEventForm = true;
    this.selectedEvent = event;
  }

  sortColumn(column, order) {
    this.sortOrder = order;
    this.EventCoordinater.controls['orderAscDesc'].setValue(order);
    this.EventCoordinater.controls['pageLength'].setValue(3);
    this.EventCoordinater.controls['pageIndex'].setValue(1);
    switch (column) {
      case 'eventName':
        this.EventCoordinater.controls['orderByColumn'].setValue(1);
        this.getEventsList();
        break;
      case 'lastModified':
        this.EventCoordinater.controls['orderByColumn'].setValue(2);
        this.getEventsList();
        break;
      case 'startDate':
        this.EventCoordinater.controls['orderByColumn'].setValue(3);
        this.getEventsList();
        break;
    }
  }
}


