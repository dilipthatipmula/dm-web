import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import {Ng2CompleterModule} from 'ng2-completer';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header.component';
import { LeftnavComponent } from './layout/leftnav.component';
import { FooterComponent } from './layout/footer.component';
import { LoginComponent } from './logins/login.component';
import { UsersComponent } from './components/admin/users.component';
import { ServicesComponent } from './components/admin/services.component';
import { ClientsComponent } from './components/admin/clients.component';
import { ClientsstaffComponent } from './components/admin/clientsstaff.component';
import { MaindashboardComponent } from './components/maindashboard.component';
import { DashboardComponent } from './components/dashboard.component';
import { NewuserComponent } from './components/admin/newuser.component';
import { NewserviceComponent } from './components/admin/newservice.component';
import { NewclientComponent } from './components/admin/newclient.component';
import { NewcstaffComponent } from './components/admin/newcstaff.component';
import { CoordinaterEventComponent } from './components/coordnator/coordinater-event.component';
import { DmexcutiveComponent } from './components/dmexcutive/dmexcutive.component';
import { ClientadminComponent } from './components/clientadmin.component';
import { TokenizedInterceptor } from 'Tokenized-Interceptor';
import { AuthService } from './shared/services/auth.service';
import { AuthGuard } from './shared/services/auth.guard';
import { CooEditEventComponent } from './components/coordnator/coo-edit-event.component';
import { CooNewEventComponent } from './components/coordnator/coo-new-event.component';
import { DMnewComponent } from './components/dmexcutive/dmnew.component';
import { EventsComponent } from './events/events.component';
import { AddEventComponent } from './events/add-event.component';



const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'maindash', component: MaindashboardComponent, children: [
      { path: '', component: DashboardComponent },
      { path: 'maindash', component: DashboardComponent },
      { path: 'users', component: UsersComponent },
      { path: 'clients', component: ClientsComponent },
      { path: 'services', component: ServicesComponent },
      { path: 'newservices', component: NewserviceComponent },
      { path: 'cstaff', component: ClientsstaffComponent },
      { path: 'newuser', component: NewuserComponent },
      { path: 'newclients', component: NewclientComponent },
      { path: 'newcstaff', component: NewcstaffComponent },
      { path: 'coordinaterevent', component: CoordinaterEventComponent },
      { path: 'coordinaterevent/:id', component: CooEditEventComponent },
      { path: 'cooEventEdit', component: CooEditEventComponent },
      { path: 'cooEventNew', component: CooNewEventComponent },
      { path: 'dmexcutivenet', component: DmexcutiveComponent },
      { path: 'dmexcutivenet/:id', component: DMnewComponent },
      { path: 'dmnew', component: DMnewComponent },
      { path: 'clientadminevent', component: ClientadminComponent },
      { path: 'events', component: EventsComponent },
    ]
  },

  { path: '**', redirectTo: '/login' }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftnavComponent,
    FooterComponent,
    LoginComponent,
    UsersComponent,
    ServicesComponent,
    ClientsComponent,
    ClientsstaffComponent,
    MaindashboardComponent,
    DashboardComponent,
    NewuserComponent,
    NewserviceComponent,
    NewclientComponent,
    NewcstaffComponent,
    CoordinaterEventComponent,
    DmexcutiveComponent,
    ClientadminComponent,
    CooEditEventComponent,
    CooNewEventComponent,
    DMnewComponent,
    EventsComponent,
    AddEventComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2CompleterModule,
    AutocompleteLibModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    AuthGuard,
    {provide: HTTP_INTERCEPTORS, useClass: TokenizedInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
