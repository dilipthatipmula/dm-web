import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  constructor(private _service:AuthService,
              private _router:Router) { }

  ngOnInit() {
  }
  logout() {
    this._service.LogOut();
    this._router.navigate(['/login'])
  }

}
