import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';
import { LoginUser } from '../shared/models/login-user';
import { AppResponse } from '../shared/models/app-response';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isSubmitted = false;
  error: string;

  constructor(private _fb: FormBuilder,
    private _service: AuthService,
    private _router: Router) { }

  ngOnInit() {
    this.loginForm = this._fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get frm() { return this.loginForm.controls; }

  onSubmit() {
    this.isSubmitted = true;
    this.error = null;
    if (!this.loginForm.valid) { return; }
    this._service.Login(this.loginForm.value)
      .subscribe((data: LoginUser) => {
        console.log('Login data ' + JSON.stringify(data));
        this._service.ManageSession(data);
        this._service.loginStatus.emit(true);
        console.log('this._service', this._service);
        if (this._service.redirectUrl) {
          this._router.navigate([this._service.redirectUrl]);
        } else {
          this._router.navigate(['/maindash']);
        }
      }, (error: AppResponse) => {
        if (error.status === 400) {
          this.error = 'Either user name or password is incorrect!';
        } else {
          this.error = error.message;
        }
      });
  }

  focused() { this.error = ''; }
}
