import { Injectable } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { HttpRequest, HttpInterceptor, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, finalize, mergeMap } from 'rxjs/operators'
import { AppResponse } from "src/app/shared/models/app-response";

@Injectable()
export class TokenizedInterceptor implements HttpInterceptor {

    constructor(private _service: AuthService) { }

    private setHeaders(request: HttpRequest<any>) {
        const token = this._service.getToken;
        if (token) {
            request = request.clone({
                setHeaders: {
                    Accept: `application/json`,
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
        }
        return request;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = this.setHeaders(request);
        return next.handle(request)
            .pipe(catchError((error: any) => {
                if (error instanceof HttpErrorResponse) {
                    switch (error.status) {
                        case 0:
                            return throwError({ status: error.status, message: "Unable to connect to server, please contact admin" } as AppResponse)
                        case 400:
                        case 401:
                        case 403:
                        case 404:
                        case 500:
                        default:
                            return throwError({ status: error.status, message: error.message } as AppResponse)
                    }
                } else if (error.error instanceof ErrorEvent) {
                    return throwError({ status: error.status, message: error.error.message } as AppResponse)
                } else {
                    return throwError({ status: error.status, message: error.error.message } as AppResponse)
                }
            }),
                finalize(() => {

                })
            )
    }
}